Sites using Acquia Search typically have one Apache Solr Core/Index assigned to
them, shared across multiple environments. To prevent duplicate search results,
the Apache Solr environment is configured in "Read-only" mode on all-but-one of
the environments (usually the production site).

This can make it difficult to test new Apache Solr-related features or
configurations in the development- or staging-environment and can cause issues
when content is not synchronized across the different environments.

This module adds an additional field (ss_ah_environment) containing the
environment identifier when indexing content, and limits returned query results
by that field. This essentially divides a shared index into sections per (Acquia
Hosting) environment.

Installation
------------
Enable the module. The module does not require any configuration.

Drawbacks
---------
- By enabling this module, each environment (local, development, staging and
production) stores all documents within its own section of the index, causing
the index to be larger (typically 2 - 3 times as big) then would be the case if
you configure one index and prevents write operations on all but one
environment.
- Deleting an index via admin/config/search/apachesolr/settings/<env_id>/index
will only delete the content for the active environment. If you enable this
module when the index is not empty, you will no longer be able to clear the
content already present and not tagged with an environment id. While this
content will not be returned as part of search results, it does waste space in
the index.

Further reading
---------------
https://docs.acquia.com/acquia-search/environments (*)
https://forums.acquia.com/acquia-products-and-services/acquia-search/deleted-content-stays-index (*)

(*) Requires registration.
